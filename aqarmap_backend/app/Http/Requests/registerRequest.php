<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Validator;

class registerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        Validator::extend('image_ext',function($attribute, $value, $params, $validator) {
            // $image = base64_decode($value);
            if(!$value) return true;
            $f = finfo_open();
            $pos  = strpos($value, ';');
            $result = explode(':', substr($value, 0, $pos))[1];
            // $result = finfo_buffer($f, $image, FILEINFO_MIME_TYPE);
            if($result == 'image/png' || $result ==  'image/jpg' || $result ==  'image/jpeg'){
                return true;
            }
            return false;
        },'image must be jpg , jpeg or png');

        return [
            'name'=>'required|max:25|min:4',
            'image'=>'image_ext',
            'password'=>'required:min:6|max:10',
            'email'=>'required|email|unique:users',
            // 'userType_id'=>'required|exists:userType,id',
            'c_password' => 'required|same:password',
        ];
    }
}
