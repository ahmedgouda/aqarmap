<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Validator;

class postRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        Validator::extend('image_ext',function($attribute, $value, $params, $validator) {
            // $image = base64_decode($value);
            if(!$value) return true;
            $f = finfo_open();
            $pos  = strpos($value, ';');
            $result = explode(':', substr($value, 0, $pos))[1];
            // $result = finfo_buffer($f, $image, FILEINFO_MIME_TYPE);
            if($result == 'image/png' || $result ==  'image/jpg' || $result ==  'image/jpeg'){
                return true;
            }
            return false;
        },'image must be jpg , jpeg or png');

        return [
            'title' => 'required|max:500|min:10',
            'body' => 'required|max:500|min:20',
            'user_id' => 'required|exists:users,id',
            'category_id' => 'required|exists:categories,id',
            'image'=>'image_ext',

        ];
    }
}
