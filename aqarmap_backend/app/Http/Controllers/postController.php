<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
use App\Http\Requests\postRequest;
use App\AuthToken;
use Image;
use App\Events\postEvent;
class postController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('admin_auth', ['only' => ['show','store','update', 'delete']]);
    }

    public function index(Request $request)
    {
        if(isset($request->category_id)){
        return Post::where('category_id', $request->category_id)->with('comments')->with('category')->with('user')->orderBy('id','desc')->paginate(5);
        }

        return Post::with('comments')->with('category')->with('user')->orderBy('id','desc')->paginate(5);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(postRequest $request)
    {
        $post =  Post::create([
            'user_id' => $request->user_id,
            'category_id' => $request->category_id,
            'title' => $request->title,
            'body' => $request->body,
            'image' => $this->upload_file($request)
        ]);
        event(new postEvent($post));
        return $post;
    }

    public function upload_file($request){
        $file_name = 'http://'.request()->getHttpHost().'/uploads/placeholder.png';
        if($request->get('image'))
        {
           $image = $request->get('image');
           $file_name = time().'.' . explode('/', explode(':', substr($image, 0, strpos($image, ';')))[1])[1];
           \Image::make($request->get('image'))->save('uploads/posts/'.$file_name);
           $file_name = 'http://'.request()->getHttpHost().'/uploads/posts/'.$file_name;
         }
        return $file_name;
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return Post::find($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(postRequest $request, $id)
    {
        return Post::find($id)->update([
            'user_id' => $request->user_id,
            'title' => $request->title,
            'body' => $request->body,
            'category_id' => $request->category_id,
            'image' => $this->upload_file($request)
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $done = Post::destroy($id);
         $res = new \stdClass;
         $res->count = $done;
         $res->status = $done ? 200 : 412;

         return  response([$res], $res->status);
    }

}
