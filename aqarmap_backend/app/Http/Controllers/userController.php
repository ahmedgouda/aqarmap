<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\AuthToken;
// use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Http\Requests\registerRequest;
use App\Http\Requests\loginRequest;
use function GuzzleHttp\json_decode;

class userController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public $successStatus = 200;
    public function __construct()
    {
        // $this->middleware('permission:manage-posts', ['only' => ['create']]);
    }

    public function index()
    {
        return User::get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(registerRequest $request)
    {
        // return $this->upload_file($request);
       $user =  User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
            'image' => $this->upload_file($request),
            'userType_id' => $request->userType_id ? $request->userType_id : 2,
        ]);
        $token = $this->createToken($user);
        $res = new \stdClass;
            $res->status = 200;
            $res->user = $user;
            $res->token  = $token;
            $res->msg = 'success login';
            return response([$res], $this->successStatus); 
    }

    public function upload_file($request)
    {
        $file_name = '';
        if($request->get('image'))
        {
           $image = $request->get('image');
           $file_name = time().'.' . explode('/', explode(':', substr($image, 0, strpos($image, ';')))[1])[1];
           \Image::make($request->get('image'))->save('uploads/users/'.$file_name);
           $file_name = 'http://'.request()->getHttpHost().'/uploads/users/'.$file_name;
         }
        return $file_name;
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return User::find($id);
    }

    public function login(loginRequest $request)
    {
        if (\Auth::check()) {
            return response([\Auth::user()], $this->successStatus);
        }
        $email = $request->email;
        $password = $request->password;

        $logged_in_user = [
            'email' => $email,
            'password' => $password,
        ];

        if (\Auth::attempt($logged_in_user)) {
            $user = \Auth::user();
            $token = $this->createToken($user);
            $res = new \stdClass;
            $res->status = 200;
            $res->user = $user;
            $res->token  = $token;
            $res->msg = 'success login';
            return response([$res], $this->successStatus);
        }

        $res = new \stdClass;
        $res->status = 401;
        $res->msg = 'failed to login';
        return response([$res], $res->status);
    }
    public function createToken($user)
    {
        $token = sha1(md5($user->id . $user->email));

        AuthToken::updateOrCreate([
            'user_id' => $user->id,
            'token' => $token,
            'expired' => 1
        ]);

        return $token;
    }

    public function check_loggedin(Request $request)
    {
        $user = AuthToken::where('token', $request->token)->first();
        if ($user && $user->count() && $user->token != null) {
            $user  = AuthToken::where('token', $request->token)->with('user')->first()->user;
            return response([$user], $this->successStatus);
        }
        return response(json_decode('[{}]'), $this->successStatus);
    }

    public function logout(Request $request)
    {
        AuthToken::where('token', $request->token)->update([
            'token' => null,
            'expired' => 0
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    { }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        return $request;
        // return User::find($id)->update([
        //     $request->except([])
        // ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $done = User::destroy($id);
        $res = new \stdClass;
        $res->count = $done;
        $res->status = $done ? $this->successStatus : 412;

        return  response([$res], $res->status);
    }
}
