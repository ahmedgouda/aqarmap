<?php

namespace App\Http\Middleware;

use Closure;

class secureRequest
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function make_safe($variable) {
        $variable = strip_tags($variable);
        $variable = stripslashes($variable);
        $variable= trim($variable, "'");
          return $variable;
      }

    public function handle($request, Closure $next)
    {
        if(isset($request->body)){
            $request->body = $this->make_safe($request->body);
        }

        if(isset($request->email)){
            $request->email = $this->make_safe($request->email);
        }
        if(isset($request->password)){
            $request->password = $this->make_safe($request->password);
        }
        if(isset($request->user_id)){
            $request->user_id = $this->make_safe($request->user_id);
        }
        if(isset($request->name)){
            $request->name = $this->make_safe($request->name);
        }


        return $next($request);
    }
}
