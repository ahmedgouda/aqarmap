<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use App\AuthToken;
class authUser
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
       $count = AuthToken::where('token', $request->token)->where('user_id', $request->user_id)->count();
       if(!$count){
           return response([], 401);
       }
        return $next($request);
    }
}
