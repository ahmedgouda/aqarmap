<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use App\User;

class RedirectIfNotAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!isset($request->user_id)) {
            return response([],401);
         }
        $user_id  = $request->user_id;

        $user = User::find($user_id);
        
        if ($user->userType_id != '1') {
          return response(['user'=>$user],401);
        }

        return $next($request);
    }
}
