<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;


    protected $fillable = [ 'name', 'email', 'password', 'userType_id', 'image'];

    protected $hidden = ['password'];
    public $timestamps = true;

    public function userType(){

        return $this->belongsTo('App\UserType');

    }

    public function Comments(){

        return $this->hasMany('App\Comment');

    }

    public function Posts(){

        return $this->hasMany('App\Post')->orderBy('id','desc');

    }
    public function token(){

        return $this->hasOne('App\AuthToken');

    }
}
