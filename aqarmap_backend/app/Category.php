<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    public $timestamps = true;
    protected $fillable =['name'];

    public function Post(){

        return $this->hasMany('App\Post')->orderBy('id','desc');

    }
}
