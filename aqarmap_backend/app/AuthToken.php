<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class AuthToken extends Model
{
    protected $table = 'auth_token';
    public $timestamps = true;
    protected $fillable =['token','user_id','expired'];

    public function user(){
        return $this->belongsTo('App\User');
    }
}
