<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    public $timestamps = true;
    protected $fillable =['post_id','user_id','body'];

    public function Post(){

        return $this->belongsTo('App\Post');

    }

    public function User(){

        return $this->belongsTo('App\User');

    }
}
