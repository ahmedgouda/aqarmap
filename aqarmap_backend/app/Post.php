<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    public $timestamps = true;

    protected $fillable =['user_id', 'category_id' ,'title','body','image'];
    //

    public function comments(){

        return $this->hasMany('App\Comment')->with('user')->orderBy('id','desc');

    }
    public function category(){

        return $this->belongsTo('App\Category');

    }
    public function user(){

        return $this->belongsTo('App\User');
    }
}
