<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });
Route::group(['middleware' => 'secure_request'], function() {
    Route::post('login', 'userController@login');
    Route::post('logout', 'userController@logout');
    Route::post('check_loggedin', 'userController@check_loggedin');

    Route::resource('users', 'userController');
    Route::resource('categories', 'categoryController');
    Route::resource('posts', 'postController');
    Route::resource('comments', 'commentController');
});
