<?php

use Illuminate\Database\Seeder;
use App\User;
class adminSeeds extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = [
            [
            'id' => 1,
            'name' => 'ahmedgouda',
            'email' => 'ahmedmoud@ymail.com',
            'password' => bcrypt(123456),
            'userType_id' => 1,
            'image' => 'http://localhost:8000/uploads/users/ahmedgouda201902255977.png',
            'created_at'=>date("Y-m-d h:i:s"),
            'updated_at'=>date("Y-m-d h:i:s")

            ],
        ];
        User::insert($users);
    }
}
