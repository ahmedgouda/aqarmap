<?php

use Illuminate\Database\Seeder;
use App\UserType;
class userTypes extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user_types = [
            ['id' => 1, 'role' => 'admin', 'created_at'=>date("Y-m-d h:i:s"),'updated_at'=>date("Y-m-d h:i:s")],
            ['id' => 2, 'role' => 'visitor', 'created_at'=>date("Y-m-d h:i:s"), 'updated_at'=>date("Y-m-d h:i:s")],
        ];
        UserType::insert($user_types);
    }
}
