import Vue from 'vue'
import Vuex from 'vuex'
import VueProgressBar from 'vue-progressbar'
import VueResource from 'vue-resource'
import api from '../model/api.js'

const options = {
  color: '#ff895d',
  failedColor: '#ff895d',
  thickness: '5px',
  transition: {
    speed: '0.1s',
    opacity: '0.9s',
    termination: 300
  },
  autoRevert: true,
  location: 'top',
  inverse: false
}
Vue.use(VueProgressBar, options)
Vue.config.productionTip = false
Vue.use(VueResource)
Vue.use(Vuex)

export const store = new Vuex.Store({
  state: {
    active_user: {},
    posts: [],
    categories: []
  },
  getters: {
    posts: state => {
      return state.posts
    },
    categories: state => {
      return state.categories
    },
    active_user: state => {
      return state.active_user
    }
  },
  mutations: {
    putLoggedinUser: (state, $data) => {
      localStorage.token = $data.token
      state.active_user = $data.user
    },
    getLoggedinUser: (state, $data) => {
      state.active_user = $data
    },
    getPosts: (state, $data) => {
      state.posts = $data
    },
    getCategories: (state, $data) => {
      state.categories = $data
    },
    registerNewUser: (state, $data) => {
      localStorage.token = $data.token
      state.active_user = $data.user
    },
    addNewPost: (state, $data) => {
      // state.posts = state.posts.concat($data).unique()
    }
  },
  actions: {
    putLoggedinUser: (context, $data) => {
      return new Promise((resolve, reject) => {
        api.login($data).then(res => {
          context.commit('putLoggedinUser', res.data[0])
          resolve('/')
        }).catch(error => {
          reject(error)
        })
      })
    },
    getLoggedinUser: (context) => {
      return new Promise((resolve, reject) => {
        var $data = { token: localStorage.token }
        api.getLoggedinUser($data).then(res => {
          context.commit('getLoggedinUser', res.data[0])
          resolve(res.data[0])
        }).catch(error => {
          reject(error)
        })
      })
    },
    getPosts: (context) => {
      return new Promise((resolve, reject) => {
        var $data = { token: localStorage.token }
        api.getPosts($data).then(res => {
          context.commit('getPosts', res.data)
          resolve(res.data)
        }).catch(error => {
          reject(error)
        })
      })
    },
    getCategories: (context) => {
      return new Promise((resolve, reject) => {
        api.getCategories().then(res => {
          context.commit('getCategories', res.data)
          resolve(res.data)
        }).catch(error => {
          reject(error)
        })
      })
    },
    registerNewUser: (context, $data) => {
      return new Promise((resolve, reject) => {
        api.registerNewUser($data).then(res => {
          context.commit('registerNewUser', res.data[0])
          resolve('/')
        }).catch(error => {
          reject(error)
        })
      })
    },
    addNewPost: (context, $data) => {
      $data.token = localStorage.token
      return new Promise((resolve, reject) => {
        api.addNewPost($data).then(res => {
          context.commit('addNewPost', res.data[0])
          resolve('/')
        }).catch(error => {
          reject(error)
        })
      })
    },
    paginate: (context, $data) => {
      return new Promise((resolve, reject) => {
        api.paginate($data).then(res => {
          context.commit('getPosts', res.data)
        }).catch(error => {
          reject(error)
        })
      })
    },
    filter: (context, $data) => {
      return new Promise((resolve, reject) => {
        api.filter($data).then(res => {
          context.commit('getPosts', res.data)
        }).catch(error => {
          reject(error)
        })
      })
    },
    createComment: (context, $data) => {
      return new Promise((resolve, reject) => {
        api.createComment($data).then(res => {

        }).catch(error => {
          reject(error)
        })
      })
    }//,
  }
})
