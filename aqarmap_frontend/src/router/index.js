import Vue from 'vue'
import Router from 'vue-router'
import Posts from '@/components/Posts'
import ContactUs from '@/components/ContactUs'
import Login from '@/components/Login'
import Register from '@/components/Register'
import AddPost from '@/components/AddPost'
import SinglePost from '@/components/SinglePost'
import { store } from '../vuex/store'

Vue.use(Router)

let router = new Router({
  routes: [
    {
      path: '/',
      name: 'Posts',
      component: Posts
    },
    {
      path: '/post/:index',
      name: 'SinglePost',
      component: SinglePost
    },
    {
      path: '/AddPost',
      name: 'AddPost',
      component: AddPost
    },
    {
      path: '/contactus',
      name: 'ContactUs',
      component: ContactUs
    },
    {
      path: '/login',
      name: 'Login',
      component: Login
    },
    {
      path: '/register',
      name: 'Register',
      component: Register
    }
  ]
})

router.beforeEach((to, from, next) => {
  if (localStorage.token !== undefined && (to.name === 'Login' || to.name === 'Register')) {
    next('/')
  }
  else if (store.state.active_user.userType_id !== 1  && (to.name === 'AddPost' || to.name === 'deletePost')) {
    next('/')
  }
  else {
    next()
  }
})
export default router
