import axios from 'axios'

const baseUrl = 'http://localhost:8000'
// const headers = {
//   'Content-Type': 'multipart/form-data'
// }
const apiObject = {
  login: function ($data) {
    return axios.post(baseUrl + '/api/login', $data)
  },
  getLoggedinUser: function ($data) {
    return axios.post(baseUrl + '/api/check_loggedin', $data)
  },
  registerNewUser: function ($data) {
    return axios.post(baseUrl + '/api/users', $data)
  },
  getCategories: function () {
    return axios.get(baseUrl + '/api/categories')
  },
  getPosts: function () {
    return axios.get(baseUrl + '/api/posts')
  },
  addNewPost: function ($data) {
    return axios.post(baseUrl + '/api/posts', $data)
  },
  createComment: function ($data) {
    return axios.post(baseUrl + '/api/comments', $data)
  },
  paginate: function ($data) {
    return axios.get($data)
  },
  filter: function ($data) {
    return axios.get(baseUrl + '/api/posts?category_id=' + $data)
  }
}
export default apiObject
